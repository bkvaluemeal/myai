# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.2.0] - Festival TTS Adapter
### Added
- Festival TTS adapter
- Bot super class

### Changed
- Process function to include listening

### Removed
- Listen function from Google TTS adapter
- Listen function from Google STT adapter

## 0.1.2 - Updated tests and docs
### Changed
- Unit tests
- Docs

## 0.1.1 - Basic Bot
### Added
- Basic Bot
- Logging

### Changed
- Refactored speech module into separate adapters

## [0.1.0] - Speech
### Added
- Speech module

[0.1.0]: https://bitbucket.org/bkvaluemeal/myai/issues/1/speech
