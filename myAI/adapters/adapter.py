import logging

class Adapter(object):
	"""
	The framework for an adapter
	"""

	def __init__(self):
		self.logger = logging.getLogger('myAI')
		self.logger.debug('Loading %s adapter' % (self.__class__.__name__))
