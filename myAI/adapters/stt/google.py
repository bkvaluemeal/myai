from myAI.adapters import Adapter
import subprocess
import requests
import json

LANGUAGES = {
	'af' : 'Afrikaans',
	'sq' : 'Albanian',
	'ar' : 'Arabic',
	'hy' : 'Armenian',
	'ca' : 'Catalan',
	'zh' : 'Chinese',
	'zh-cn' : 'Chinese (Mandarin/China)',
	'zh-tw' : 'Chinese (Mandarin/Taiwan)',
	'zh-yue' : 'Chinese (Cantonese)',
	'hr' : 'Croatian',
	'cs' : 'Czech',
	'da' : 'Danish',
	'nl' : 'Dutch',
	'en' : 'English',
	'en-au' : 'English (Australia)',
	'en-uk' : 'English (United Kingdom)',
	'en-us' : 'English (United States)',
	'eo' : 'Esperanto',
	'fi' : 'Finnish',
	'fr' : 'French',
	'de' : 'German',
	'el' : 'Greek',
	'ht' : 'Haitian Creole',
	'hi' : 'Hindi',
	'hu' : 'Hungarian',
	'is' : 'Icelandic',
	'id' : 'Indonesian',
	'it' : 'Italian',
	'ja' : 'Japanese',
	'ko' : 'Korean',
	'la' : 'Latin',
	'lv' : 'Latvian',
	'mk' : 'Macedonian',
	'no' : 'Norwegian',
	'pl' : 'Polish',
	'pt' : 'Portuguese',
	'pt-br' : 'Portuguese (Brazil)',
	'ro' : 'Romanian',
	'ru' : 'Russian',
	'sr' : 'Serbian',
	'sk' : 'Slovak',
	'es' : 'Spanish',
	'es-es' : 'Spanish (Spain)',
	'es-us' : 'Spanish (United States)',
	'sw' : 'Swahili',
	'sv' : 'Swedish',
	'ta' : 'Tamil',
	'th' : 'Thai',
	'tr' : 'Turkish',
	'vi' : 'Vietnamese',
	'cy' : 'Welsh'
}

class stt(Adapter):
	"""
	Speech-to-Text (STT) is an interface to Google's STT api

	Args:
		lang (str): The language to use (default: en-us)
	"""

	def __init__(self, lang = 'en-us'):
		super().__init__()

		if lang.lower() not in LANGUAGES:
			raise Exception('Language not supported: %s' % lang)
		else:
			self.lang = lang.lower()

		self.get_profile()
		self.logger.debug('Loaded Google STT adapter')

	def process(self, speech = None):
		"""
		Processes a given speech to text if speech is provided, or
		listens for speech

		Args:
			speech (bin): An optional binary flac to process
		"""

		if not speech:
			p = subprocess.Popen(
				'rec -c 1 -r 48k -q -t flac - noisered - 0.21 silence -l 1 0.0 1% 1 3.0 5% pad 1 0',
				stdin = subprocess.PIPE,
				stdout = subprocess.PIPE,
				stderr = subprocess.DEVNULL,
				shell = True
			)
			speech, stderr = p.communicate(self.profile)

		url = 'http://www.google.com/speech-api/v2/recognize?client=chromium&lang=%s&key=AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw' % (self.lang)
		headers = {'Content-Type': 'audio/x-flac; rate=48000'}
		response = requests.post(url, data = speech, headers = headers).text

		result = None
		for line in response.split('\n'):
			try:
				result = json.loads(line)['result'][0]['alternative'][0]['transcript']
				break
			except:
				pass

		return result

	def convert(self, speech, format = 'mp3'):
		"""
		Converts audio of the format, format, to flac

		Args:
			speech (bin): The binary audio to convert
			format (str): The format of the audio (default = mp3)
		"""

		p = subprocess.Popen(
			'sox -t %s - -t flac - rate 48k' % (format),
			stdin = subprocess.PIPE,
			stdout = subprocess.PIPE,
			shell = True
		)
		stdout, stderr = p.communicate(speech)

		return stdout

	def get_profile(self):
		"""
		Records a 5 second sample for noise reduction
		"""

		self.logger.info('Recording noise profile')
		self.profile = subprocess.check_output(
			'rec -c 1 -r 48k -q -n noiseprof - trim 0 5',
			stderr = subprocess.DEVNULL,
			shell = True
		)
