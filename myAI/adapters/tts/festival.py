from myAI.adapters import Adapter
import subprocess

LANGUAGES = {
	'en' : 'English',
	'en-au' : 'English (Australia)',
	'en-uk' : 'English (United Kingdom)',
	'en-us' : 'English (United States)',
}

class tts(Adapter):
	"""
	Text-to-speech (TTS) adapter for Festival
	"""

	def __init__(self, lang = 'en-us'):
		super().__init__()

		if lang.lower() not in LANGUAGES:
			raise Exception('Language not supported: %s' % lang)
		else:
			self.lang = lang.lower()

		self.logger.debug('Loaded Festival TTS adapter')

	def process(self, text = None, file_path = None):
		"""
		Processes a given text to speech and optionally save it

		Args:
			text (str): The text to process
			file_path (str): An optional file path to save the audio
				- A file path of 'stdout' will return the audio
		"""

		if not text:
			return

		if file_path:
			if file_path == 'stdout':
				file_path = '-'

			p = subprocess.Popen(
				'text2wave -o %s' % (file_path),
				stdin = subprocess.PIPE,
				stdout = subprocess.PIPE,
				stderr = subprocess.DEVNULL,
				shell = True
			)
			return p.communicate(str.encode(text))
		else:
			subprocess.Popen(
				'festival --tts',
				stdin = subprocess.PIPE,
				stderr = subprocess.DEVNULL,
				shell = True
			).communicate(str.encode(text))
