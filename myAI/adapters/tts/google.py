from myAI.adapters import Adapter
import subprocess
import calendar
import requests
import math
import nltk
import time

LANGUAGES = {
	'af' : 'Afrikaans',
	'sq' : 'Albanian',
	'ar' : 'Arabic',
	'hy' : 'Armenian',
	'ca' : 'Catalan',
	'zh' : 'Chinese',
	'zh-cn' : 'Chinese (Mandarin/China)',
	'zh-tw' : 'Chinese (Mandarin/Taiwan)',
	'zh-yue' : 'Chinese (Cantonese)',
	'hr' : 'Croatian',
	'cs' : 'Czech',
	'da' : 'Danish',
	'nl' : 'Dutch',
	'en' : 'English',
	'en-au' : 'English (Australia)',
	'en-uk' : 'English (United Kingdom)',
	'en-us' : 'English (United States)',
	'eo' : 'Esperanto',
	'fi' : 'Finnish',
	'fr' : 'French',
	'de' : 'German',
	'el' : 'Greek',
	'ht' : 'Haitian Creole',
	'hi' : 'Hindi',
	'hu' : 'Hungarian',
	'is' : 'Icelandic',
	'id' : 'Indonesian',
	'it' : 'Italian',
	'ja' : 'Japanese',
	'ko' : 'Korean',
	'la' : 'Latin',
	'lv' : 'Latvian',
	'mk' : 'Macedonian',
	'no' : 'Norwegian',
	'pl' : 'Polish',
	'pt' : 'Portuguese',
	'pt-br' : 'Portuguese (Brazil)',
	'ro' : 'Romanian',
	'ru' : 'Russian',
	'sr' : 'Serbian',
	'sk' : 'Slovak',
	'es' : 'Spanish',
	'es-es' : 'Spanish (Spain)',
	'es-us' : 'Spanish (United States)',
	'sw' : 'Swahili',
	'sv' : 'Swedish',
	'ta' : 'Tamil',
	'th' : 'Thai',
	'tr' : 'Turkish',
	'vi' : 'Vietnamese',
	'cy' : 'Welsh'
}

class tts(Adapter):
	"""
	Text-to-Speech (TTS) is an interface to Google's TTS api

	Args:
		lang (str): The language to use (default: en-us)
	"""

	def __init__(self, lang = 'en-us'):
		super().__init__()

		if lang.lower() not in LANGUAGES:
			raise Exception('Language not supported: %s' % lang)
		else:
			self.lang = lang.lower()

		self.token_key = None
		self.logger.debug('Loaded Google TTS adapter')

	def process(self, text = None, file_path = None):
		"""
		Processes a given text to speech and optionally save it

		Args:
			text (str): The text to process
			file_path (str): An optional file path to save the audio
				- A file path of 'stdout' will return the audio
		"""

		if not text:
			return
		else:
			text = nltk.sent_tokenize(text)

		result = b''
		for idx, part in enumerate(text):
			payload = {
				'ie' : 'UTF-8',
				'q' : part,
				'tl' : self.lang,
				'total' : len(text),
				'idx' : idx,
				'client' : 't',
				'textlen' : len(part),
				'tk' : self._token(part)
			}
			headers = {
				'Referer' : 'http://translate.google.com/',
				'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36'
			}

			try:
				r = requests.get('https://translate.google.com/translate_tts', params = payload, headers = headers)
				r.raise_for_status()

				for chunk in r.iter_content(chunk_size = 1024):
					result += chunk
			except Exception as e:
				raise

		if file_path:
			if file_path == 'stdout':
				return result
			else:
				with open(file_path, 'wb') as file:
					file.write(result)
		else:
			subprocess.Popen(
				'play -t mp3 - -q',
				stdin = subprocess.PIPE,
				stderr = subprocess.DEVNULL,
				shell = True
			).communicate(result)

	def _rshift(self, val, n):
		return val>>n if val >= 0 else (val+0x100000000)>>n

	def _work_token(self, a, salt):
		for i in range(0, len(salt) - 2, 3):
			char = salt[i + 2]
			d = ord(char[0]) - 87 if char >= 'a' else int(char)
			d = self._rshift(a, d) if salt[i + 1] == '+' else a << d
			a = a + d & 4294967295 if salt[i] == '+' else a ^ d

		return a

	def _token(self, text, seed = None):
		if self.token_key is None and seed is None:
			timestamp = calendar.timegm(time.gmtime())
			hours = int(math.floor(timestamp / 3600))
			self.token_key = hours

		e = 0
		f = 0
		d = [None] * len(text)

		for c in text:
			g = ord(c)

			if 128 > g:
				d[e] = g
				e += 1
			elif 2048 > g:
				d[e] = g >> 6 | 192
				e += 1
			else:
				if 55296 == (g & 64512) and f + 1 < len(text) and 56320 == (ord(text[f + 1]) & 64512):
					f += 1
					g = 65536 + ((g & 1023) << 10) + (ord(text[f]) & 1023)
					d[e] = g >> 18 | 240
					e += 1
					d[e] = g >> 12 & 63 | 128
					e += 1
				else:
					d[e] = g >> 12 | 224
					e += 1
					d[e] = g >> 6 & 63 | 128
					e += 1
					d[e] = g & 63 | 128
					e += 1

		a = seed if seed is not None else self.token_key

		if seed is None:
			seed = self.token_key

		for value in d:
			a += value
			a = self._work_token(a, '+-a^+6')

		a = self._work_token(a, '+-3^+b+-f')

		if 0 > a:
			a = (a & 2147483647) + 2147483648

		a %= 1E6
		a = int(a)

		return str(a) + '.' + str(a ^ seed)
